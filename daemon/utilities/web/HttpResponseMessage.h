#pragma once

#include <boost/asio/awaitable.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <memory>
#include <optional>

namespace bxt::Utilities::Web {

namespace {
    namespace beast = boost::beast; // from <boost/beast.hpp>
    namespace http = beast::http;   // from <boost/beast/http.hpp>
    namespace net = boost::asio;

} // namespace

template<class AsyncReadStream>
class HttpResponseMessage {
    using AsyncReadStreamPtr = std::unique_ptr<AsyncReadStream>;

public:
    HttpResponseMessage(AsyncReadStreamPtr connection)
        : m_stream(std::move(connection))
    {
        static_assert(beast::is_async_read_stream<AsyncReadStream>::value,
                      "AsyncReadStream type requirements not met");

        if (!m_stream->is_open())
            throw std::runtime_error("HTTP connection not open");

        http::read_header(*m_stream, m_buffer, m_parser);
    }

    bool is_chuncked() const { return m_parser.chunked(); }
    bool is_keep_alive() const { return m_parser.keep_alive(); }
    const auto& headers() const { return m_parser.get().base(); }

    boost::optional<uint64_t> content_length()
    {
        return m_parser.content_length();
    }

    template<typename TCallback>
    net::awaitable<void>
        copy_to(std::ostream& stream, beast::error_code& ec, TCallback& cb)
    {
        while (!m_parser.is_done()) {
            char temp_buffer[512];

            m_parser.get().body().data = temp_buffer;
            m_parser.get().body().size = sizeof(temp_buffer);

            co_await http::read(*m_stream, m_buffer, m_parser, ec);

            if (ec == http::error::need_buffer) ec = {};
            if (ec) co_return;

            auto bytes_transferred =
                sizeof(temp_buffer) - m_parser.get().body().size;

            stream.write(temp_buffer, bytes_transferred);

            cb(bytes_transferred, content_length().value());
        }
    }

    virtual ~HttpResponseMessage() = default;

private:
    AsyncReadStreamPtr m_stream;
    beast::flat_buffer m_buffer;
    http::response_parser<http::buffer_body> m_parser;
};

} // namespace bxt::Utilities::Web