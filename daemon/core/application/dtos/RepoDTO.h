/* === This file is part of bxt ===
 *
 *   SPDX-FileCopytightText: 2022 Andrey Solovyev <freelyn-info@ya.ru>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#pragma once

#include "core/domain/entities/Architecture.h"

#include <boost/uuid/uuid.hpp>
#include <set>

namespace bxt::Core::Application
{

struct RepoDTO {
    boost::uuids::uuid id;
    std::set<Domain::Architecture> architectures;
};

} // namespace bxt::Core::Application