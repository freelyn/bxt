/* === This file is part of bxt ===
 *
 *   SPDX-FileCopytightText: 2022 Andrey Solovyev <freelyn-info@ya.ru>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#pragma once

#include "core/domain/entities/Permission.h"
#include "core/domain/value_objects/Name.h"

#include <boost/uuid/uuid.hpp>
#include <set>

namespace bxt::Core::Application
{

struct UserDTO {
    boost::uuids::uuid id;
    Domain::Name name;
    std::string password;
    std::set<Domain::Permission> permissions;
};

} // namespace bxt::Core::Application