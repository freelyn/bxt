/* === This file is part of bxt ===
 *
 *   SPDX-FileCopytightText: 2022 Andrey Solovyev <freelyn-info@ya.ru>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#pragma once

#include "core/domain/value_objects/Name.h"
#include "core/domain/value_objects/PackageArchitecture.h"
#include "core/domain/value_objects/PackageVersion.h"

#include <boost/uuid/uuid.hpp>

namespace bxt::Core::Application
{

struct PackageDTO {
    boost::uuids::uuid id;
    Domain::Name name;
    Domain::PackageVersion version;
    Domain::PackageArchitecture architecture;
};

} // namespace bxt::Core::Application