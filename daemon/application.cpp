/* === This file is part of bxt ===
 *
 *   SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#include <boost/asio/awaitable.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/beast.hpp>
#include <fmt/format.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <utilities/web/HttpResponseMessage.h>

namespace beast = boost::beast; // from <boost/beast.hpp>
namespace http = beast::http;   // from <boost/beast/http.hpp>
namespace net = boost::asio;    // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;

net::awaitable<void> make_request(net::io_context& ioc)
{
    namespace web = bxt::Utilities::Web;

    std::string host = "173.203.57.63";
    auto const port = "80";

    tcp::resolver resolver {ioc};
    http::request<http::empty_body> request(http::verb::get, "/", 11);

    auto sock = std::make_unique<tcp::socket>(ioc);

    auto resolve_results =
        co_await resolver.async_resolve(host, port, net::use_awaitable);

    co_await net::async_connect(*sock, resolve_results, net::use_awaitable);

    co_await http::async_write(*sock, request, net::use_awaitable);

    beast::error_code ec;

    web::HttpResponseMessage<tcp::socket> resp(std::move(sock));

    if (resp.is_keep_alive()) { std::cout << "Keep Alive\n"; }

    if (resp.content_length().has_value()) {
        std::cout << "Content-Leght: " << resp.content_length().get() << "\n";
    }

    std::cout << "\n";

    for (const auto& field : resp.headers()) {
        std::cout << "Field: " << field.name_string() << " -> " << field.value()
                  << "\n";
    }

    std::ofstream stream("./test.html", std::ios::app);

    auto size = 0;

    auto callback = [&](size_t bytes_transferred, size_t totalsize) {
        std::cout << "Bytes transferred: " << bytes_transferred
                  << "| Total size: " << totalsize << "\n";

        size += bytes_transferred;
    };

    co_await resp.copy_to(stream, ec, callback);

    std::cout << "Total size: " << size;

    stream.close();
}

int main()
{
    net::io_context ioc;

    net::co_spawn(
        ioc, [&] { return make_request(ioc); }, net::detached);

    ioc.run();

    return 0;
}
