# **bxt**  -  ALPM  repository  manager

`bxt` (**B**o**x**I**t**)  is  a  repository  management  system  that  allows  you  to  create  repositories  in  a  declarative  way  and  operate  with  it  using a command line tool or a web-API.

This repository contains following projects:

- daemon (bxtd) - a server side backend application that handles all the repository creation and interaction with clients.
